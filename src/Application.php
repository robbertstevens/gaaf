<?php

namespace Ji;

use Ji\Blueprints\ActionInterface;
use Ji\Exceptions\ActionException;
use Ji\Exceptions\RouteNotFoundException;
use Ji\Exceptions\RouterException;
use Ji\Exceptions\RouterNotSetException;
use Ji\Http\RouteCollection;
use Ji\Http\Router;
use Ji\Http\Url;
use Psr\Container\ContainerInterface;

class Application
{
    protected $router;
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Application constructor.
     * @param RouteCollection $routes
     * @param ContainerInterface $container
     */
    private function __construct(RouteCollection $routes, ContainerInterface $container)
    {
        $router = new Router($routes);
        $this->setRouter($router);
        $this->container = $container;
    }

    /**
     * @throws RouteNotFoundException
     */
    public function run()
    {
        $router = $this->getRouter();

        /** @var Url $route */
        $route = $router->dispatch();

        if (is_null($route)) {
            throw RouterException::routeNotFound();
        }

        try {
            $object = new \ReflectionClass($route->getAction());
        } catch (\ReflectionException $exception) {
            throw $exception;
        }

        if (!$object->implementsInterface(ActionInterface::class)) {
            throw ActionException::wrongInterface();
        }

        $injectables = $this->getDependencies($object->getConstructor());

        $instance = $object->newInstanceArgs($injectables);

        $instance->execute(...$route->getParameters());
        unset($instance);
    }

    /**
     * @param Router $router
     */
    public function setRouter(Router $router)
    {
        $this->router = $router;
    }


    /**
     * @return mixed
     * @throws RouterException
     */
    public function getRouter()
    {
        if (!isset($this->router)) {
            throw RouterException::routerNotSet();
        }

        return $this->router;
    }

    /**
     * @param $routes
     * @param $container
     * @return Application
     */
    public static function create(
        RouteCollection $routes,
        ContainerInterface $container
    )
    {
        return new Application($routes, $container);
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return $this->container;
    }


    /**
     * @param \ReflectionMethod $method
     * @return array
     */
    protected function getDependencies(\ReflectionMethod $method)
    {
        $injectables = [];
        if (is_null($method)) {
            return $injectables;
        }

        foreach ($method->getParameters() as $parameter) {
            $className = $parameter->getClass()->getName();
            $injectables[] = $this->getContainer()->get($className);
        }
        return $injectables;
    }
}