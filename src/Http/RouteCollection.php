<?php

namespace Ji\Http;

class RouteCollection
{
    /** @var Url[] */
    protected $collection;

    /**
     * RouteCollection constructor.
     * @param array $collection
     */
    public function __construct(array $collection)
    {
        $this->setCollection($collection);
    }

    /**
     * @param Url[] $collection
     * @return RouteCollection
     */
    public function setCollection(array $collection): RouteCollection
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * @return Url[]
     */
    public function getCollection(): array
    {
        return $this->collection;
    }

    /**
     * @param $url
     * @return Url|null
     */
    public function find($url)
    {
        foreach ($this->collection as $route) {
            if ($route->match($url)) {
                return $route;
            }
        }
        return null;
    }

    /**
     * @param Url $url
     */
    public function addUrlToCollection(Url $url)
    {
        $this->collection[] = $url;
    }
}