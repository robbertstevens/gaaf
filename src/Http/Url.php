<?php

namespace Ji\Http;


class Url
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $action;
    private $parameters;

    /**
     * Url constructor.
     * @param string $url
     * @param string $action
     */
    protected function __construct(string $url, string $action)
    {
        $this->url = $url;
        $this->action = $action;
    }

    public function execute()
    {

    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param $parameters
     */
    private function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param $url
     * @return bool
     */
    public function match($url)
    {
        $regex = sprintf("~^%s$~", $this->getUrl());

        if ($match = preg_match($regex, $url, $args)) {
            $args = array_filter($args, "is_string", ARRAY_FILTER_USE_KEY);
            $args = array_keys($args);
            $this->setParameters($args);
        }

        return (bool)$match;
    }

    /**
     * @param $url
     * @param $action
     * @return Url
     */
    public static function create($url, $action)
    {
        return new Url($url, $action);
    }

}