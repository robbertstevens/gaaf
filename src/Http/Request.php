<?php

namespace Ji\Http;

class Request
{
    /**
     * @var array
     */
    private $get;

    /**
     * @var array
     */
    private $post;

    /**
     * @var array
     */
    private $cookies;

    /**
     * @var array
     */
    private $files;

    /**
     * @var array
     */
    private $session;

    /**
     * @var array
     */
    private $server;

    /**
     * Request constructor.
     * @param array $get The GET parameters
     * @param array $post The POST parameters
     * @param array $cookies The COOKIES parameters
     * @param array $files The FILES parameters
     * @param array $session The SESSION parameters
     * @param array $server
     * @internal param array $query The GET parameters
     * @internal param array $request The POST parameters
     * @internal param array $attributes The request parameters
     * @internal param array $server The SERVER parameters
     */
    protected function __construct(
        array $get = array(),
        array $post = array(),
        array $cookies = array(),
        array $files = array(),
        array $session = array(),
        array $server = array()
    )
    {
        $this->get = $get;
        $this->post = $post;
        $this->cookies = $cookies;
        $this->files = $files;
        $this->session = $session;
        $this->server = $server;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return (string)$this->server["REQUEST_URI"] ?? "";
    }

    public static function get()
    {
        return new Request($_GET, $_POST, $_COOKIE, $_FILES, $_SESSION??[], $_SERVER??[]);
    }
}