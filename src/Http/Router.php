<?php

namespace Ji\Http;


/**
 * TODO: Find current route
 * TODO: Reverse route
 */
class Router
{
    /** @var RouteCollection */
    protected $collection;

    public function __construct(
        RouteCollection $collection
    )
    {
        $this->collection = $collection;
    }


    /**
     * @return Url|null
     */
    public function dispatch()
    {
        $request = Request::get();
        $current = $this->collection->find($request->getUrl());

        return $current;
    }
}