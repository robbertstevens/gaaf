<?php

namespace Ji\Blueprints;


interface ActionInterface
{
    public function execute();
}