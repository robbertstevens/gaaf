<?php

namespace Ji\Exceptions;


class RouterException extends \Exception
{
    /**
     * @return RouterException
     */
    public static function routerNotSet()
    {
        return new RouterException("A router has to be set before continuing");
    }

    public static function routeNotFound()
    {
        return new RouterException("No route was found!");
    }
}