<?php

namespace Ji\Exceptions;


use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class DiException extends \Exception implements ContainerExceptionInterface, NotFoundExceptionInterface
{
    public static function definitionNotFound()
    {
        return new DiException("No entry was found in the container.");
    }

    public static function generalContainerError()
    {
        return new DiException("Error while retrieving the entry.");
    }
}