<?php

namespace Ji\Exceptions;


class ActionException extends \Exception
{
    /**
     * @return ActionException
     */
    public static function wrongInterface()
    {
        return new ActionException("Implement the right interface");
    }
}