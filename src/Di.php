<?php

namespace Ji;


use Ji\Exceptions\DiException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class Di implements ContainerInterface
{
    private $definitions;

    /**
     * Di constructor.
     * @param $definitions
     */
    public function __construct($definitions)
    {
        $this->definitions = $definitions;
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get($id)
    {
        if (!$this->has($id)) {
            throw DiException::definitionNotFound();
        }

        $definition = $this->definitions[$id];
        // We know we need to create a new instance, lets DO this!
        if (is_object($definition)) {
            return $definition;
        }

        try {
            $object = new \ReflectionClass($definition);
            return $object->newInstance();
        } catch (\ReflectionException $exception) {
            throw DiException::generalContainerError();
        }


    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has($id)
    {
        return array_key_exists($id, $this->definitions);
    }
}