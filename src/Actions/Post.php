<?php

namespace Ji\Actions;

use Ji\Blueprints\ActionInterface;
use Ji\Http\Request;

class Post implements ActionInterface
{
    /**
     * @var Request
     */
    private $request;

    function __construct(
        Request $request
    )
    {
        $this->request = $request;
    }


    public function execute()
    {
        echo Post::class;
    }
}