<?php

namespace Ji\Actions;

use Ji\Blueprints\ActionInterface;
use Ji\Http\Request;
use Ji\Repositories\PostsRepositoryInterface;

class Home implements ActionInterface
{
    /**
     * @var PostsRepositoryInterface
     */
    private $repository;
    /**
     * @var Request
     */
    private $request;

    /**
     * Home constructor.
     * @param PostsRepositoryInterface $repository
     */
    function __construct(
        Request $request,
        PostsRepositoryInterface $repository
    )
    {
        $this->repository = $repository;
        $this->request = $request;
    }

    function execute()
    {
        echo Home::class;
    }
}