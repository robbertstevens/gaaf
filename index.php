<?php
/**
 * A system for the website of Robbert Stevens
 */
namespace Ji;

use Ji\Actions\Home;
use Ji\Actions\Post;
use Ji\Http\Request;
use Ji\Http\RouteCollection;
use Ji\Http\Url;
use Ji\Repositories\FilePostsRepository;
use Ji\Repositories\PostsRepositoryInterface;

require_once 'vendor/autoload.php';

$routes = new RouteCollection([
    Url::create("/", Home::class),
    Url::create("/post/(?P<id>[0-9]+)", Post::class)
]);

$definitions = [
    Request::class => Request::get(),
    PostsRepositoryInterface::class => FilePostsRepository::class,
];

$container = new Di($definitions);

try {
    Application::create($routes, $container)->run();
} catch (\Exception $e) {
    echo $e->getMessage();
}
