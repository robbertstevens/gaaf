# Ji Framework  #

A simple framework for general webdevelopment. Primarily made for learning purposes and the use on my own [website](https://robbert.rocks). 
The word Ji comes from the Chinese character 基 which means: base or foundation.

### What is this repository for? ###

* Learning basic techniques
* Usage on my [website](https://robbert.rocks)

### How do I get set up? ###

* Clone this repo
* Run `composer install`
* Start webserver

### TODO ###

* Docker
* Use full configuration setup
* The `view` and `model` part of the application
* Recursive dependency injection